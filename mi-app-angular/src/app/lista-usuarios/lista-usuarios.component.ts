import { Component, OnInit } from '@angular/core';
import { DoctoresService } from '../doctores.service';

@Component({
  selector: 'app-lista-usuarios',
  templateUrl: './lista-usuarios.component.html',
  styleUrls: ['./lista-usuarios.component.css']
})
export class ListaUsuariosComponent implements OnInit {
  doctores: any[] = [];

  constructor(private doctoresService: DoctoresService) {}

  ngOnInit() {
    this.cargarDoctores();
  }

  cargarDoctores() {
    this.doctoresService.obtenerDoctores().subscribe(data => {
      console.log(data);
      this.doctores = data as any[];
    });
    
  }

  eliminarDoctor(id: number) {
    this.doctoresService.eliminarDoctor(id).subscribe(() => {
      this.cargarDoctores();  // Recargar la lista después de eliminar
    });
  }
}
