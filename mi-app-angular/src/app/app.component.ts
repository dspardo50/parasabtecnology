import { Component, OnInit } from '@angular/core';
import { DoctoresService } from './doctores.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  doctores: any[] = [];

  constructor(private doctoresService: DoctoresService) {}

  ngOnInit() {
    this.doctoresService.obtenerDoctores().subscribe((data: any) => {
      this.doctores = data as any[];
  });
  
  }
}
