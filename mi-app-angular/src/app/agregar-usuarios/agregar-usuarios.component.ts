import { Component } from '@angular/core';
import { DoctoresService } from '../doctores.service';

@Component({
  selector: 'app-agregar-usuario', // Puedes mantener este selector así si lo prefieres
  templateUrl: './agregar-usuarios.component.html',  // Aquí es donde estaba el error
  styleUrls: ['./agregar-usuarios.component.css']   
})
export class AgregarUsuarioComponent {
  nombre: string = '';
  email: string = '';

  constructor(private doctoresService: DoctoresService) {}

  agregarDoctor() {
    const nuevoDoctor = {
      nombre: this.nombre,
      email: this.email
    };

    this.doctoresService.agregarDoctor(nuevoDoctor).subscribe(() => {
      this.nombre = '';  // Resetear el input
      this.email = '';   // Resetear el input
    });
  }
}
