const express = require('express');
const mysql = require('mysql');
const bodyParser = require('body-parser');
const cors = require('cors');  // 1. Importar CORS

const app = express();

app.use(cors());  // 2. Usar CORS como middleware antes de tus rutas

// Middleware para analizar el cuerpo de las solicitudes POST
app.use(bodyParser.json());

const db = mysql.createConnection({
    host: 'localhost',
    port: 4000,
    user: 'root',
    password: '',
    database: 'hospital'
});

db.connect((err) => {
    if (err) {
        throw err;
    }
    console.log('Conexión a la base de datos MySQL establecida');
});

// Agregar un usuario
app.post('/doctores', (req, res) => {
    const { nombre, email } = req.body;
    const query = 'INSERT INTO doctores (nombre, email) VALUES (?, ?)';
    db.query(query, [nombre, email], (err, result) => {
        if (err) throw err;
        res.send('Usuario agregado con éxito');
    });
});

// Listar los usuarios afsfdsf
app.get('/doctores', (req, res) => {
    const query = 'SELECT * FROM doctores';
    db.query(query, (err, results) => {
        if (err) throw err;
        res.json(results);
    });
});

// Eliminar un usuario
app.delete('/doctores/:id', (req, res) => {
    const userId = req.params.id;
    const query = 'DELETE FROM doctores WHERE id = ?';
    db.query(query, [userId], (err, result) => {
        if (err) throw err;
        res.send(`Usuario con ID ${userId} eliminado`);
    });
});

const PORT = 3000;
app.listen(PORT, () => {
    console.log(`Servidor corriendo en http://localhost:${PORT}`);
});
