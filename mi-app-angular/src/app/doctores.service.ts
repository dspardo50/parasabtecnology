import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class DoctoresService {
  private apiUrl = 'http://localhost:3000/doctores';

  constructor(private http: HttpClient) { }
//dasfasdfafddfas
  obtenerDoctores() {
    return this.http.get(this.apiUrl);
  }

  agregarDoctor(doctor: any) {
    return this.http.post(this.apiUrl, doctor);
  }

  eliminarDoctor(id: number) {
    return this.http.delete(`${this.apiUrl}/${id}`);
  }
}
