import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';
import { DoctoresService } from './doctores.service';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ListaUsuariosComponent } from './lista-usuarios/lista-usuarios.component';
import { AgregarUsuarioComponent } from './agregar-usuarios/agregar-usuarios.component';

@NgModule({
  declarations: [
    AppComponent,
    ListaUsuariosComponent,
    AgregarUsuarioComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule  // Añadir esta línea
  ],
  providers: [DoctoresService], 
  bootstrap: [AppComponent]
})
export class AppModule { }
